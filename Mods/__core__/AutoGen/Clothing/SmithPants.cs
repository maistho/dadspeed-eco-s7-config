﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Mods.TechTree;
    using Eco.Shared.Items;
    using Eco.Core.Items;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    
    [Serialized]
    [LocDisplayName("Smith Pants")]
    [Weight(100)]
    [Tag("Clothes", 1)]
    [Ecopedia("Items", "Clothing", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    public partial class SmithPantsItem :
        ClothingItem
    {
        public override LocString DisplayDescription  { get { return Localizer.DoStr("Denim pants with knee patches."); } }
        public override string Slot             { get { return ClothingSlot.Pants; } }
        public override bool Starter            { get { return false ; } }

    }
    

    [RequiresSkill(typeof(TailoringSkill), 5)]
    public partial class SmithPantsRecipe : RecipeFamily
    {
        public SmithPantsRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "SmithPants",  //noloc
                Localizer.DoStr("Smith Pants"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(CottonFabricItem), 40, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<SmithPantsItem>()
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 5;
            this.LaborInCalories = CreateLaborInCaloriesValue(1200, typeof(TailoringSkill));
            this.CraftMinutes = CreateCraftTimeValue(1);
            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Smith Pants"), typeof(SmithPantsRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(AdvancedTailoringTableObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}
