﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated from VehicleTemplate.cs />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Components.VehicleModules;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Math;
    using Eco.Shared.Networking;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.Items;
    
    [Serialized]
    [LocDisplayName("Steam Tractor")]
    [Weight(25000)]
    [AirPollution(0.05f)]
    [Ecopedia("Crafted Objects", "Vehicles", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    public partial class SteamTractorItem : WorldObjectItem<SteamTractorObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A tractor powered through steam."); } }
    }


    [RequiresSkill(typeof(MechanicsSkill), 2)]
    public partial class SteamTractorRecipe : RecipeFamily
    {
        public SteamTractorRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "SteamTractor",  //noloc
                Localizer.DoStr("Steam Tractor"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(IronPlateItem), 12, typeof(MechanicsSkill)),
                    new IngredientElement(typeof(IronPipeItem), 8, typeof(MechanicsSkill)),
                    new IngredientElement(typeof(ScrewsItem), 24, typeof(MechanicsSkill)),
                    new IngredientElement(typeof(LeatherHideItem), 20, typeof(MechanicsSkill)),
                    new IngredientElement("Lumber", 30, typeof(MechanicsSkill)), //noloc
                    new IngredientElement(typeof(PortableSteamEngineItem), 1, true),
                    new IngredientElement(typeof(IronWheelItem), 4, true),
                    new IngredientElement(typeof(IronAxleItem), 2, true),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<SteamTractorItem>()
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 25;
            this.LaborInCalories = CreateLaborInCaloriesValue(1000, typeof(MechanicsSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(SteamTractorRecipe), 10, typeof(MechanicsSkill));
            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Steam Tractor"), typeof(SteamTractorRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(AssemblyLineObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }

}
