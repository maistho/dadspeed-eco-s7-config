﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated from VehicleTemplate.cs />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Components.VehicleModules;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Math;
    using Eco.Shared.Networking;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.Items;
    
    [Serialized]
    [LocDisplayName("Excavator")]
    [Weight(30000)]
    [AirPollution(0.7f)]
    [Ecopedia("Crafted Objects", "Vehicles", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    public partial class ExcavatorItem : WorldObjectItem<ExcavatorObject>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("Like a Skid Steer but more versatile. Great for high slope excavation."); } }
    }


    [RequiresSkill(typeof(IndustrySkill), 2)]
    public partial class ExcavatorRecipe : RecipeFamily
    {
        public ExcavatorRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "Excavator",  //noloc
                Localizer.DoStr("Excavator"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(GearboxItem), 4, typeof(IndustrySkill)),
                    new IngredientElement(typeof(SteelPlateItem), 20, typeof(IndustrySkill)),
                    new IngredientElement(typeof(NylonFabricItem), 20, typeof(IndustrySkill)),
                    new IngredientElement(typeof(AdvancedCombustionEngineItem), 1, true),
                    new IngredientElement(typeof(RubberWheelItem), 4, true),
                    new IngredientElement(typeof(RadiatorItem), 2, true),
                    new IngredientElement(typeof(SteelAxleItem), 2, true),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<ExcavatorItem>()
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 24;
            this.LaborInCalories = CreateLaborInCaloriesValue(3000, typeof(IndustrySkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(ExcavatorRecipe), 20, typeof(IndustrySkill));
            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Excavator"), typeof(ExcavatorRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(RoboticAssemblyLineObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }

}
