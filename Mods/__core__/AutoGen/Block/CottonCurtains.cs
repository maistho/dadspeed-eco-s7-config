﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Core.Items;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;

    /// <summary>Auto-generated class. Don't modify it! All your changes will be wiped with next update! Use Mods* partial methods instead for customization.</summary>

    [RequiresSkill(typeof(TailoringSkill), 3)]
    public partial class CottonCurtainsRecipe : RecipeFamily
    {
        public CottonCurtainsRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "CottonCurtains",  //noloc
                Localizer.DoStr("Cotton Curtains"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(CottonFabricItem), 5, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                    new IngredientElement(typeof(CottonThreadItem), 2, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                    new IngredientElement("HewnLog", 2, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)), //noloc
                },
                new List<CraftingElement>
                {
                    new CraftingElement<CottonCurtainsItem>(4)
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 1.5f;
            this.LaborInCalories = CreateLaborInCaloriesValue(120, typeof(TailoringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(CottonCurtainsRecipe), 2, typeof(TailoringSkill), typeof(TailoringFocusedSpeedTalent), typeof(TailoringParallelSpeedTalent));
            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Cotton Curtains"), typeof(CottonCurtainsRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(LoomObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }

    [Serialized]
    [Solid, Wall, Constructed,BuildRoomMaterialOption]
    [RequiresSkill(typeof(TailoringSkill), 3)]
    public partial class CottonCurtainsBlock :
        Block
        , IRepresentsItem
    {
        public virtual Type RepresentedItemType { get { return typeof(CottonCurtainsItem); } }
    }

    [Serialized]
    [LocDisplayName("Cotton Curtains")]
    [MaxStackSize(20)]
    [Weight(5000)]
    [Ecopedia("Blocks", "Building Materials", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    public partial class CottonCurtainsItem :
 
    BlockItem<CottonCurtainsBlock>
    {
        public override LocString DisplayNamePlural { get { return Localizer.DoStr("Cotton Curtains"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("Curtains woven using the finest cotton. \n\n (Only cosmetic does not impact room value.)"); } }

        public override bool IgnoreRooms     { get { return true;  } }

        private static Type[] blockTypes = new Type[] {
            typeof(CottonCurtainsStacked1Block),
            typeof(CottonCurtainsStacked2Block),
            typeof(CottonCurtainsStacked3Block),
            typeof(CottonCurtainsStacked4Block)
        };
        
        public override Type[] BlockTypes { get { return blockTypes; } }
    }

    [Serialized, Solid] public class CottonCurtainsStacked1Block : PickupableBlock { }
    [Serialized, Solid] public class CottonCurtainsStacked2Block : PickupableBlock { }
    [Serialized, Solid] public class CottonCurtainsStacked3Block : PickupableBlock { }
    [Serialized, Solid,Wall] public class CottonCurtainsStacked4Block : PickupableBlock { } //Only a wall if it's all 4 CottonCurtains
}
