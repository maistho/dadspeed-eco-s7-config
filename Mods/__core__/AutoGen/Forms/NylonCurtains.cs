﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;
    




    [RotatedVariants(typeof(NylonCurtainsEdgeWallBlock), typeof(NylonCurtainsEdgeWall90Block), typeof(NylonCurtainsEdgeWall180Block), typeof(NylonCurtainsEdgeWall270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(EdgeWallFormType), typeof(NylonCurtainsItem))]
    public partial class NylonCurtainsEdgeWallBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsEdgeWall90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsEdgeWall180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsEdgeWall270Block : Block
    { }
    [RotatedVariants(typeof(NylonCurtainsEdgeWallTurnBlock), typeof(NylonCurtainsEdgeWallTurn90Block), typeof(NylonCurtainsEdgeWallTurn180Block), typeof(NylonCurtainsEdgeWallTurn270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(EdgeWallTurnFormType), typeof(NylonCurtainsItem))]
    public partial class NylonCurtainsEdgeWallTurnBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsEdgeWallTurn90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsEdgeWallTurn180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsEdgeWallTurn270Block : Block
    { }
    [RotatedVariants(typeof(NylonCurtainsThinWallCornerBlock), typeof(NylonCurtainsThinWallCorner90Block), typeof(NylonCurtainsThinWallCorner180Block), typeof(NylonCurtainsThinWallCorner270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(ThinWallCornerFormType), typeof(NylonCurtainsItem))]
    public partial class NylonCurtainsThinWallCornerBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsThinWallCorner90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsThinWallCorner180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsThinWallCorner270Block : Block
    { }
    [RotatedVariants(typeof(NylonCurtainsThinWallStraightBlock), typeof(NylonCurtainsThinWallStraight90Block), typeof(NylonCurtainsThinWallStraight180Block), typeof(NylonCurtainsThinWallStraight270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(ThinWallStraightFormType), typeof(NylonCurtainsItem))]
    public partial class NylonCurtainsThinWallStraightBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsThinWallStraight90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsThinWallStraight180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class NylonCurtainsThinWallStraight270Block : Block
    { }

}