﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;
    




    [RotatedVariants(typeof(CottonCurtainsEdgeWallBlock), typeof(CottonCurtainsEdgeWall90Block), typeof(CottonCurtainsEdgeWall180Block), typeof(CottonCurtainsEdgeWall270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(EdgeWallFormType), typeof(CottonCurtainsItem))]
    public partial class CottonCurtainsEdgeWallBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsEdgeWall90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsEdgeWall180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsEdgeWall270Block : Block
    { }
    [RotatedVariants(typeof(CottonCurtainsEdgeWallTurnBlock), typeof(CottonCurtainsEdgeWallTurn90Block), typeof(CottonCurtainsEdgeWallTurn180Block), typeof(CottonCurtainsEdgeWallTurn270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(EdgeWallTurnFormType), typeof(CottonCurtainsItem))]
    public partial class CottonCurtainsEdgeWallTurnBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsEdgeWallTurn90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsEdgeWallTurn180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsEdgeWallTurn270Block : Block
    { }
    [RotatedVariants(typeof(CottonCurtainsThinWallCornerBlock), typeof(CottonCurtainsThinWallCorner90Block), typeof(CottonCurtainsThinWallCorner180Block), typeof(CottonCurtainsThinWallCorner270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(ThinWallCornerFormType), typeof(CottonCurtainsItem))]
    public partial class CottonCurtainsThinWallCornerBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsThinWallCorner90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsThinWallCorner180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsThinWallCorner270Block : Block
    { }
    [RotatedVariants(typeof(CottonCurtainsThinWallStraightBlock), typeof(CottonCurtainsThinWallStraight90Block), typeof(CottonCurtainsThinWallStraight180Block), typeof(CottonCurtainsThinWallStraight270Block))]
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(ThinWallStraightFormType), typeof(CottonCurtainsItem))]
    public partial class CottonCurtainsThinWallStraightBlock : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsThinWallStraight90Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsThinWallStraight180Block : Block
    { }

    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    public partial class CottonCurtainsThinWallStraight270Block : Block
    { }

}