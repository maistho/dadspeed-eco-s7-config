﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;
    
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(FloorFormType), typeof(NylonCarpetItem))]
    public partial class NylonCarpetFloorBlock :
        Block, IRepresentsItem
    {
        public Type RepresentedItemType { get { return typeof(NylonCarpetItem); } }
    }
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(FullWallFormType), typeof(NylonCarpetItem))]
    public partial class NylonCarpetFullWallBlock :
        Block, IRepresentsItem
    {
        public Type RepresentedItemType { get { return typeof(NylonCarpetItem); } }
    }
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(SimpleFloorFormType), typeof(NylonCarpetItem))]
    public partial class NylonCarpetSimpleFloorBlock :
        Block, IRepresentsItem
    {
        public Type RepresentedItemType { get { return typeof(NylonCarpetItem); } }
    }
    [Serialized]
    [Wall, Constructed, Solid, BuildRoomMaterialOption]
    [BlockTier(4)]
    [IsForm(typeof(CubeFormType), typeof(NylonCarpetItem))]
    public partial class NylonCarpetCubeBlock :
        Block, IRepresentsItem
    {
        public Type RepresentedItemType { get { return typeof(NylonCarpetItem); } }
    }





}