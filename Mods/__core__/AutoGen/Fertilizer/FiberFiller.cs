﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Core.Items;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using System.ComponentModel;


    [RequiresSkill(typeof(FertilizersSkill), 2)]
    public partial class FiberFillerRecipe : RecipeFamily
    {
        public FiberFillerRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "FiberFiller",  //noloc
                Localizer.DoStr("Fiber Filler"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(PlantFibersItem), 15, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                    new IngredientElement(typeof(DirtItem), 1, typeof(FertilizersSkill), typeof(FertilizersLavishResourcesTalent)),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<FiberFillerItem>()
                });
            this.Recipes = new List<Recipe> { recipe };
            this.LaborInCalories = CreateLaborInCaloriesValue(15, typeof(FertilizersSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(FiberFillerRecipe), 0.3f, typeof(FertilizersSkill), typeof(FertilizersFocusedSpeedTalent), typeof(FertilizersParallelSpeedTalent));
            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Fiber Filler"), typeof(FiberFillerRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(FarmersTableObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
    
    [Serialized]
    [LocDisplayName("Fiber Filler")]
    [Weight(500)]
    [Category("Tool")]
    [Tag("Fertilizer", 1)]
    [Ecopedia("Items", "Fertilizer", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
        [Tag("FertilizerFiller", 1)]
    public partial class FiberFillerItem : FertilizerItem<FiberFillerItem>
    {
        public override LocString DisplayDescription { get { return Localizer.DoStr("A filler ingredient used in a variety of fertilizers."); } }

        static FiberFillerItem()
        {
            nutrients = new List<NutrientElement>();
            nutrients.Add(new NutrientElement("Nitrogen", 0.3f));
            nutrients.Add(new NutrientElement("Phosphorus", 0.4f));
            nutrients.Add(new NutrientElement("Potassium", 0.2f));
        }
    }
}
