﻿﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Blocks;
    using Eco.Shared.Localization;

    public class RectangleFill : BlockFill
    {
        public override int SortOrder => 3;
        public override string Name => "Rectangle";
        public override LocString DisplayName => Localizer.DoStr("Rectangle");
        public override LocString DisplayDescription => Localizer.DoStr("A flat rectangle of blocks.");
        public override int HammerTier => 2;
    }
}
