﻿﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Blocks;
    using Eco.Shared.Localization;

    public class Box3PointFill : BlockFill
    {
        public override int SortOrder => 8;
        public override string Name => "Box3Point";
        public override LocString DisplayName => Localizer.DoStr("Box 3 Point");
        public override LocString DisplayDescription => Localizer.DoStr("A full box.");
        public override int HammerTier => 4;
    }
}
