﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Core.Items;
    using Eco.Gameplay.Animals;
    using Eco.Mods.Organisms;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Simulation.Agents;
    using Eco.Simulation.Types;
    using Eco.Mods.Organisms.Behaviors;

    /// <summary>Auto-generated class. All your changes will be wiped with next update!</summary>
    public class Tarantula : AnimalEntity
    {
        public Tarantula(Animal parent, Vector3 pos, bool corpse = false) : base(parent, pos, species, corpse) { }
        static AnimalSpecies species;

        [Ecopedia("Animals", "Invertebrates", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
        [Localized(false, true)]
        public class TarantulaSpecies : AnimalSpecies
        {
            public TarantulaSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Tarantula);

                // Info
                this.Name = "Tarantula"; //noloc
                this.DisplayName = Localizer.DoStr("Tarantula");
                // Lifetime
                this.MaturityAgeDays = 1;
                // Food
                this.FoodSources = new List<System.Type>() {  };
                this.CalorieValue = 100;
                // Movement
                this.WanderingSpeed = 0.7f;
                this.Speed = 1;
                this.ClimbHeight = 0;
                // Resources
                this.ResourceBonusAtGrowth = 0.9f;
                // Behavior
                this.BrainType = typeof(LandPredatorBrain);
                this.IsPredator = true;
                this.Health = 1;
                this.Damage = 3;
                this.ChanceToAttack = 0.6f;
                this.DelayBetweenAttacksRangeSec = new Range(0.8f, 2.5f);
                this.FearFactor = 1;
                this.FleePlayers = true;
                this.AttackRange = 1;
                this.HeadDistance = 0.1f;
                this.TimeLayToStand = 4;
                this.TimeAttackToIdle = 1;
                // Climate
                this.ReleasesCO2TonsPerDay = 0.02f;
                // Tags
                this.Tags = new[] { "spider" };
            }


        }


        public override (bool, string) ShouldSleep => RelaxBehaviors.DaySleeper;
    }
}
