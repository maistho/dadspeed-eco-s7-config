﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated from WorldObjectTemplate.tt />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Modules;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;
    using Eco.Gameplay.Housing.PropertyValues;
    using Eco.Gameplay.Civics.Objects;

    [Serialized]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(SolidGroundComponent))]
    public partial class CompositeSpruceDoorObject : DoorObject, IRepresentsItem
    {
        public override Type RepresentedItemType => typeof(CompositeSpruceDoorItem);
        public override LocString DisplayName => Localizer.DoStr("Composite Spruce Door");
        public override TableTextureMode TableTexture => TableTextureMode.Wood;
        public override bool HasTier => true;
        public override int Tier => 4;

        protected override void Initialize()
        {
            this.ModsPreInitialize();
            base.Initialize();

            this.ModsPostInitialize();
        }

        public override void Destroy()
        {
            base.Destroy();
        }

        /// <summary>Hook for mods to customize WorldObject before initialization. You can change housing values here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize WorldObject after initialization.</summary>
        partial void ModsPostInitialize();
    }

    [Serialized]
    [LocDisplayName("Composite Spruce Door")]
    [Tier(4)]
    [Ecopedia("Housing Objects", "Doors", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    public partial class CompositeSpruceDoorItem : WorldObjectItem<CompositeSpruceDoorObject>
    {
        public override LocString DisplayDescription => Localizer.DoStr("A beautiful composite door built by an expert carpenter.");


    }

    [RequiresSkill(typeof(CompositesSkill), 1)]
    public partial class CompositeSpruceDoorRecipe : Recipe
    {
        public CompositeSpruceDoorRecipe()
        {
            this.Init(
                "CompositeSpruceDoor",  //noloc
                Localizer.DoStr("Composite Spruce Door"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(CompositeSpruceLumberItem), 2, typeof(CompositesSkill), typeof(CompositesLavishResourcesTalent)),
                    new IngredientElement(typeof(ScrewsItem), 4, typeof(CompositesSkill), typeof(CompositesLavishResourcesTalent)),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<CompositeSpruceDoorItem>()
                });
            this.ModsPostInitialize();
            CraftingComponent.AddTagProduct(typeof(AdvancedCarpentryTableObject), typeof(CompositeLumberDoorRecipe), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}
