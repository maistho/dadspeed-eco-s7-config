﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated from WorldObjectTemplate.tt />

namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Core.Items;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Modules;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;
    using Eco.Gameplay.Housing.PropertyValues;
    using Eco.Gameplay.Civics.Objects;
    using static Eco.Gameplay.Housing.PropertyValues.HomeFurnishingValue;

    [Serialized]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(HousingComponent))]
    [RequireComponent(typeof(SolidGroundComponent))]
    public partial class RugLargeObject : WorldObject, IRepresentsItem
    {
        public virtual Type RepresentedItemType => typeof(RugLargeItem);
        public override LocString DisplayName => Localizer.DoStr("Rug Large");
        public override TableTextureMode TableTexture => TableTextureMode.Canvas;

        protected override void Initialize()
        {
            this.ModsPreInitialize();
            this.GetComponent<HousingComponent>().HomeValue = RugLargeItem.HomeValue;
            this.ModsPostInitialize();
        }

        public override void Destroy()
        {
            base.Destroy();
        }

        /// <summary>Hook for mods to customize WorldObject before initialization. You can change housing values here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize WorldObject after initialization.</summary>
        partial void ModsPostInitialize();
    }

    [Serialized]
    [LocDisplayName("Rug Large")]
    [Ecopedia("Housing Objects", "Decoration", createAsSubPage: true, display: InPageTooltip.DynamicTooltip)]
    [Tag("Housing", 1)]
    public partial class RugLargeItem : WorldObjectItem<RugLargeObject>
    {
        public override LocString DisplayDescription => Localizer.DoStr("A large area rug to cover that weird stain.");
        public override LocString DisplayNamePlural => Localizer.DoStr("Large Rugs");

        [TooltipChildren] public HomeFurnishingValue HousingTooltip { get { return HomeValue; } }
        public static readonly HomeFurnishingValue HomeValue = new HomeFurnishingValue()
        {
            Category                 = RoomCategory.General,
            SkillValue               = 2,
            TypeForRoomLimit         = Localizer.DoStr("Rug"),
            DiminishingReturnPercent = 0.5f
        };

    }

    [RequiresSkill(typeof(TailoringSkill), 6)]
    public partial class RugLargeRecipe : RecipeFamily
    {
        public RugLargeRecipe()
        {
            var recipe = new Recipe();
            recipe.Init(
                "RugLarge",  //noloc
                Localizer.DoStr("Rug Large"),
                new List<IngredientElement>
                {
                    new IngredientElement(typeof(ClothItem), 20, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                    new IngredientElement(typeof(CelluloseFiberItem), 15, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                    new IngredientElement(typeof(WoolYarnItem), 40, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                },
                new List<CraftingElement>
                {
                    new CraftingElement<RugLargeItem>()
                });
            this.Recipes = new List<Recipe> { recipe };
            this.ExperienceOnCraft = 3;
            this.LaborInCalories = CreateLaborInCaloriesValue(120, typeof(TailoringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(RugLargeRecipe), 10, typeof(TailoringSkill), typeof(TailoringFocusedSpeedTalent), typeof(TailoringParallelSpeedTalent));
            this.ModsPreInitialize();
            this.Initialize(Localizer.DoStr("Rug Large"), typeof(RugLargeRecipe));
            this.ModsPostInitialize();
            CraftingComponent.AddRecipe(typeof(LoomObject), this);
            CraftingComponent.AddRecipe(typeof(AutomaticLoomObject), this);
        }

        /// <summary>Hook for mods to customize RecipeFamily before initialization. You can change recipes, xp, labor, time here.</summary>
        partial void ModsPreInitialize();
        /// <summary>Hook for mods to customize RecipeFamily after initialization, but before registration. You can change skill requirements here.</summary>
        partial void ModsPostInitialize();
    }
}
