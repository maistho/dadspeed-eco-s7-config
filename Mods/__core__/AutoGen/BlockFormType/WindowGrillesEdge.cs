﻿﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
// <auto-generated />

namespace Eco.Mods.TechTree
{
    using System;
    using Eco.Gameplay.Blocks;
    using Eco.Shared.Localization;
    using Eco.World.Blocks;

    public partial class WindowGrillesEdgeFormType : FormType
    {
        public override string Name => "WindowGrillesEdge";
        public override LocString DisplayName => Localizer.DoStr("Window Grilles Edge");
        public override LocString DisplayDescription => Localizer.DoStr("Window Grilles Edge");
        public override Type GroupType => typeof(ThinFormGroup);
        public override int SortOrder => 93;
        public override int MinTier => 1;
    }
}
