﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Eco.Mods.TechTree
{
    using Eco.Core.Utils;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Simulation.WorldLayers;
    using Eco.Simulation.WorldLayers.Layers;
    using Eco.World;
    using Eco.World.Blocks;
    using System;
    using System.Collections.Generic;

    public partial class WaterwheelItem : WorldObjectItem<WaterwheelObject>
    {
        public override Type[] Blockers { get { return AllowWaterPlacement; } }             // This allows the waterwheel to be placed inside of water.
    }

    public partial class WaterwheelObject : WorldObject
    {
        static WaterwheelObject()
        {                                                                                   // Some blocks intentionally left empty for the purposes of water-movement.
            WorldObject.AddOccupancy<WaterwheelObject>(new List<BlockOccupancy>(){
            new BlockOccupancy(new Vector3i(0, -3, -3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -3, -2), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -3, -1), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -3, 0), typeof(EmptyBlock)),                 // Bottom block.
            new BlockOccupancy(new Vector3i(0, -3, 1), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -3, 2), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -3, 3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, -3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, -2), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, -1), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, 0), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, 1), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, 2), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -2, 3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, -3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, -2), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, -1), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, 0), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, 1), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, 2), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, -1, 3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, 0, -3), typeof(EmptyBlock)),                 // Left block.
            new BlockOccupancy(new Vector3i(0, 0, -2)),
            new BlockOccupancy(new Vector3i(0, 0, -1)),
            new BlockOccupancy(new Vector3i(0, 0, 0), typeof(EmptyBlock)),                  // Center block.
            new BlockOccupancy(new Vector3i(0, 0, 1)),
            new BlockOccupancy(new Vector3i(0, 0, 2)),
            new BlockOccupancy(new Vector3i(0, 0, 3), typeof(EmptyBlock)),                  // Right block.
            new BlockOccupancy(new Vector3i(0, 1, -3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, 1, -2)),
            new BlockOccupancy(new Vector3i(0, 1, -1)),
            new BlockOccupancy(new Vector3i(0, 1, 0)),
            new BlockOccupancy(new Vector3i(0, 1, 1)),
            new BlockOccupancy(new Vector3i(0, 1, 2)),
            new BlockOccupancy(new Vector3i(0, 1, 3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, 2, -3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, 2, -2)),
            new BlockOccupancy(new Vector3i(0, 2, -1)),
            new BlockOccupancy(new Vector3i(0, 2, 0)),
            new BlockOccupancy(new Vector3i(0, 2, 1)),
            new BlockOccupancy(new Vector3i(0, 2, 2)),
            new BlockOccupancy(new Vector3i(0, 2, 3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, 3, -3), typeof(EmptyBlock)),
            new BlockOccupancy(new Vector3i(0, 3, -2)),
            new BlockOccupancy(new Vector3i(0, 3, -1)),
            new BlockOccupancy(new Vector3i(0, 3, 0), typeof(EmptyBlock)),                  // Top block.
            new BlockOccupancy(new Vector3i(0, 3, 1)),
            new BlockOccupancy(new Vector3i(0, 3, 2)),
            new BlockOccupancy(new Vector3i(0, 3, 3), typeof(EmptyBlock)),
            });
        }
    }
}
