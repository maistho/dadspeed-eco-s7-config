﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Eco.Mods.TechTree
{
    using Eco.Gameplay.Housing;
    using Eco.Core.Items;
    using Eco.Core.Plugins.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Settlements;
    using Eco.Shared.Localization;

    //Definitions for properties of the settlements system
    public class SettlementDefinitions : IModInit
    {
        public static void Initialize()
        {
            //                                                                  Town, Country, Federation
            SettlementConfig.Obj.InfluenceRadii                 = new float[] { 50, 100, 1000 };
            SettlementConfig.Obj.LandClaimsPerCitizenPastFirst  = new float[] { 10, 10, 10 };
            SettlementConfig.Obj.ClaimStakesPerCitizenPastFirst = new float[] { .5f, .5f, .5f };
            SettlementConfig.Obj.ClosestAllowedNeighborDistance = new float[] { 10, 20, 50 };
            SettlementConfig.Obj.HoursToResolvePropertyCrisis   = new float[] { 24, 24, 24 };
            

            SettlementUtils.TownLeaderNames       = new[] { Localizer.DoStr("Mayor") };
            SettlementUtils.CountryLeaderNames    = new[] { Localizer.DoStr("President"), Localizer.DoStr("Chief Executive"), Localizer.DoStr("Czar"), Localizer.DoStr("Sultan"), Localizer.DoStr("Prime Minister"), Localizer.DoStr("Head of State"), Localizer.DoStr("Premier"), Localizer.DoStr("Chancellor") };
            SettlementUtils.FederationLeaderNames = new[] { Localizer.DoStr("Scretary-General") };
            
            SettlementUtils.TownDefaultSuffixes = new[]
            {
                Localizer.DoStr("ville"),
                Localizer.DoStr(" Town"),
                Localizer.DoStr("sdale"),
                Localizer.DoStr("burg"),
                Localizer.DoStr("shire"),
                Localizer.DoStr("berg"),
                Localizer.DoStr("borough"),
                Localizer.DoStr("field"),
                Localizer.DoStr("tropolis"),
                Localizer.DoStr(" Square"),
                Localizer.DoStr("rock"),
                Localizer.DoStr("spire"),
                Localizer.DoStr("bury"),
                Localizer.DoStr("borough"),
                Localizer.DoStr(" Garden"),
                Localizer.DoStr("glen"),
                Localizer.DoStr("stead"),
                Localizer.DoStr("'s Landing"),
            };


            SettlementUtils.CountryDefaultSuffixes = new[]
            {
                Localizer.DoStr("land"),
            };

            SettlementUtils.FederationDefaultSuffixes = new[]
            {
                Localizer.DoStr("United Nations"),
                Localizer.DoStr("League of Nations"),
                Localizer.DoStr("Federation of Earth"),
            };
        }
    }
}
