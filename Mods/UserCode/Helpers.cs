namespace Eco.Mods
{
    using System.Text;
    using System.Linq;
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    using Eco.Shared.Localization;
    using Eco.Shared.Utils;
    public class ObjectDumper
    {
        public static void WriteLine<T>(T obj)
        {
            var t = typeof(T);
            if (t.GetProperties().Any())
            {
                var props = t.GetProperties();
                StringBuilder sb = new StringBuilder();
                foreach (var item in props)
                {
                    sb.Append($"{item.Name}:{item.GetValue(obj, null)}; ");
                }
                sb.AppendLine();
                Log.WriteLine(Localizer.DoStr(sb.ToString()));
            }
        }
    }
}

namespace Eco.Mods
{
    using System;
    using Eco.Gameplay.Items;
    public static class RecipeExtensions
    {
        public static void ReplaceTagIngredient(this Recipe recipe, string tag, IngredientElement ingredient)
        {
            recipe.Ingredients[
                recipe.Ingredients.FindIndex(i => i.Tag?.Name == tag)
            ] = ingredient;
        }
        public static void ReplaceIngredient(this Recipe recipe, Type type, IngredientElement ingredient)
        {
            recipe.Ingredients[
                recipe.Ingredients.FindIndex(i => i.Item.Type == type)
            ] = ingredient;

        }
        public static void ReplaceOutput(this Recipe recipe, Type type, CraftingElement element)
        {
            recipe.Items[
                recipe.Items.FindIndex(i => i.Item.Type == type)
            ] = element;
        }
        public static void RemoveOutput(this Recipe recipe, Type type)
        {
            recipe.Items.RemoveAt(
                recipe.Items.FindIndex(i => i.Item.Type == type)
            );
        }
    }
}