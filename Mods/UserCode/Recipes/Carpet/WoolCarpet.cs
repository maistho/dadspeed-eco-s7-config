﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class WoolCarpetRecipe : RecipeFamily
    {
        partial void ModsPreInitialize()
        {
            /// Double ingredient costs
            Recipes[0].Ingredients.Clear();
            Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
                new IngredientElement(typeof(EpoxyItem), 2, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                new IngredientElement(typeof(WoolFabricItem), 10, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)),
                new IngredientElement("Lumber", 2, typeof(TailoringSkill), typeof(TailoringLavishResourcesTalent)), //noloc
            });
            /// Double xp, labor and craft time
            this.ExperienceOnCraft *= 2;
            this.LaborInCalories = CreateLaborInCaloriesValue(360, typeof(TailoringSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(WoolCarpetRecipe), 4, typeof(TailoringSkill), typeof(TailoringFocusedSpeedTalent), typeof(TailoringParallelSpeedTalent));
        }
    }
}