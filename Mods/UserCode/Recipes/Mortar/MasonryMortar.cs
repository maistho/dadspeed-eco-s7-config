
namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class MasonryMortarRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Decrease Mortar output from 3 to 1
            Recipes[0].ReplaceOutput(
                typeof(MortarItem),
                new CraftingElement<MortarItem>(1)
            );
        }
    }
}
