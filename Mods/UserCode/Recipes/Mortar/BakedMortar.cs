﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class BakedMortarRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Increase Wood Pulp consumption from 5 to 12
            Recipes[0].ReplaceIngredient(
                typeof(WoodPulpItem),
                new IngredientElement(typeof(WoodPulpItem), 12, typeof(MasonrySkill), typeof(MasonryLavishResourcesTalent))
            );
            /// Increase calorie consumption from 60 to 100
            this.LaborInCalories = CreateLaborInCaloriesValue(100, typeof(MasonrySkill));
        }
    }
}
