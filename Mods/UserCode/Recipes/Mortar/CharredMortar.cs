﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class CharredMortarRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Increase NaturalFiber consumption from 5 to 20
            Recipes[0].ReplaceTagIngredient(
                "NaturalFiber",
                new IngredientElement("NaturalFiber", 20, true) //noloc
            );
            /// Increase calorie consumption from 15 to 45
            this.LaborInCalories = CreateLaborInCaloriesValue(45);
        }
    }
}
