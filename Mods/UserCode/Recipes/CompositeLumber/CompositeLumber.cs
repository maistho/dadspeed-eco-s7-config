﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;

    public partial class CompositeLumberRecipe
    {
        partial void ModsPreInitialize()
        {
            // Increase amount of wood and lumber to bring costs up more in line with Ashlar
            Recipes[0].ReplaceTagIngredient(
                "Wood",
                new IngredientElement("Wood", 4, typeof(CompositesSkill), typeof(CompositesLavishResourcesTalent)) //noloc
            );
            Recipes[0].ReplaceTagIngredient(
                "Lumber",
                new IngredientElement("Lumber", 2, typeof(CompositesSkill), typeof(CompositesLavishResourcesTalent)) //noloc
            );

        }
    }
}
