﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;

    public partial class CompositeOakLumberRecipe
    {
        partial void ModsPostInitialize()
        {
            // Increase amount of wood and lumber to bring costs up more in line with Ashlar
            this.ReplaceIngredient(
                typeof(OakLogItem),
                new IngredientElement(typeof(OakLogItem), 4, typeof(CompositesSkill), typeof(CompositesLavishResourcesTalent)) //noloc
            );
            this.ReplaceTagIngredient(
                "Lumber",
                new IngredientElement("Lumber", 2, typeof(CompositesSkill), typeof(CompositesLavishResourcesTalent)) //noloc
            );
        }
    }
}