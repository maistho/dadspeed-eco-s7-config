﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class RubberWheelRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Double all ingredients
            Recipes[0].Ingredients.Clear();
            Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
                new IngredientElement(typeof(SyntheticRubberItem), 16, typeof(IndustrySkill), typeof(IndustryLavishResourcesTalent)),
                new IngredientElement(typeof(SteelBarItem), 8, typeof(IndustrySkill), typeof(IndustryLavishResourcesTalent)),
            });
            /// Double xp labor and craft time
            this.ExperienceOnCraft *= 2;
            this.LaborInCalories = CreateLaborInCaloriesValue(120, typeof(IndustrySkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(RubberWheelRecipe), 4, typeof(IndustrySkill), typeof(IndustryFocusedSpeedTalent), typeof(IndustryParallelSpeedTalent));
        }
    }
}