﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class AsphaltConcreteRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Increase CrushedRock usage from 5 -> 10
            Recipes[0].ReplaceTagIngredient(
                "CrushedRock",
                new IngredientElement("CrushedRock", 10, typeof(BasicEngineeringSkill), typeof(BasicEngineeringLavishResourcesTalent)) //noloc
            );
        }
    }
}
