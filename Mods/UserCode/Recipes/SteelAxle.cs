﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class SteelAxleRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Double all ingredients
            Recipes[0].Ingredients.Clear();
            Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
                new IngredientElement(typeof(SteelBarItem), 8, typeof(IndustrySkill), typeof(IndustryLavishResourcesTalent)),
                new IngredientElement(typeof(EpoxyItem), 6, typeof(IndustrySkill), typeof(IndustryLavishResourcesTalent)),
            });
            /// Double xp labor and craft time
            this.ExperienceOnCraft *= 2;
            this.LaborInCalories = CreateLaborInCaloriesValue(120, typeof(IndustrySkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(SteelAxleRecipe), 3.0f, typeof(IndustrySkill), typeof(IndustryFocusedSpeedTalent), typeof(IndustryParallelSpeedTalent));
        }
    }
}