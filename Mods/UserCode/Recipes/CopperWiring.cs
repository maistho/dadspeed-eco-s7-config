﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class CopperWiringRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Add 4 Cotton Fabric to Ingredients
            Recipes[0].Ingredients.Add(
                new IngredientElement(typeof(CottonFabricItem), 4, typeof(MechanicsSkill), typeof(MechanicsLavishResourcesTalent))
            );
        }
    }
}