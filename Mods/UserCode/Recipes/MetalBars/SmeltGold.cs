﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class SmeltGoldRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Add 3 static clay to act as early game molds
            Recipes[0].Ingredients.Add(
                new IngredientElement(typeof(ClayItem), 3, true)
            );

        }
    }
}