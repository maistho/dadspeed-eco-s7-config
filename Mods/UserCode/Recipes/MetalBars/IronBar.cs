﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class IronBarRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Double recipe from 1->2 to 2->4
            Recipes[0].ReplaceIngredient(
                typeof(IronConcentrateItem),
                new IngredientElement(typeof(IronConcentrateItem), 2, typeof(SmeltingSkill), typeof(SmeltingLavishResourcesTalent))
            );
            Recipes[0].ReplaceOutput(
                typeof(IronBarItem),
               new CraftingElement<IronBarItem>(4)
            );
            Recipes[0].ReplaceOutput(
                typeof(SlagItem),
                new CraftingElement<SlagItem>(typeof(SmeltingSkill), 2, typeof(SmeltingLavishResourcesTalent))
            );
            this.ExperienceOnCraft *= 2;
            this.LaborInCalories = CreateLaborInCaloriesValue(120, typeof(SmeltingSkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(IronBarRecipe), 0.6f, typeof(SmeltingSkill), typeof(SmeltingFocusedSpeedTalent), typeof(SmeltingParallelSpeedTalent));

            /// Add 1 ceramic mold to bring more business to Pottery
            Recipes[0].Ingredients.Add(
                new IngredientElement("Ceramic Mold", 1, typeof(SmeltingSkill), typeof(SmeltingLavishResourcesTalent))
            );

        }
    }
}
