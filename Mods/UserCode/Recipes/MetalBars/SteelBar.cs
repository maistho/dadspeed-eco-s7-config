﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class SteelBarRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Add 1 ceramic mold to bring more business to Pottery
            Recipes[0].Ingredients.Add(
                new IngredientElement("Ceramic Mold", 1, typeof(SmeltingSkill), typeof(SmeltingLavishResourcesTalent))
            );

        }
    }
}