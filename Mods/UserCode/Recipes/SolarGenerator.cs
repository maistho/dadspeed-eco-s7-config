﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;

    public partial class SolarGeneratorRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Make recipe a lot more expensive
            /// Remove bonus from Upgrades/Skill Level
            this.Recipes[0].Ingredients.Clear();
            this.Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
               new IngredientElement(typeof(SteelPlateItem), 60, true),
               new IngredientElement(typeof(ServoItem), 24, true),
               new IngredientElement(typeof(BasicCircuitItem), 24, true),
                /// Add Framed Glass to make it more expensive and give some extra business to Glass Working
               new IngredientElement(typeof(FramedGlassItem), 24, true),
               new IngredientElement(typeof(GoldWiringItem), 40, true),
            });

            /// Increase Exp by around 5x to compensate slightly for increased costs
            this.ExperienceOnCraft *= 5;

            /// Increase calories required by 4x
            this.LaborInCalories = CreateLaborInCaloriesValue(2400, typeof(ElectronicsSkill));

            /// Increase craft time by 4x
            this.CraftMinutes = CreateCraftTimeValue(typeof(SolarGeneratorRecipe), 80, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));
        }
    }
}
