﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class AdvancedCombustionEngineRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Make all ingredients static
            Recipes[0].Ingredients.Clear();
            Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
                new IngredientElement(typeof(SteelPlateItem), 16, true),
                new IngredientElement(typeof(RivetItem), 12, true),
                new IngredientElement(typeof(PistonItem), 6, true),
                new IngredientElement(typeof(ValveItem), 6, true),
                new IngredientElement(typeof(ServoItem), 6, true),
                new IngredientElement(typeof(AdvancedCircuitItem), 6, true),
                new IngredientElement(typeof(RadiatorItem), 3, true),
            });
        }
    }
}