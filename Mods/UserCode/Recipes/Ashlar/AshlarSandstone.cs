﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;

    public partial class AshlarSandstoneRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Use reinforced concrete instead of steel bars to increase use of CrushedRock and Sand
            Recipes[0].ReplaceIngredient(
                typeof(SteelBarItem),
                new IngredientElement(typeof(ReinforcedConcreteItem), 2, typeof(AdvancedMasonrySkill), typeof(AdvancedMasonryLavishResourcesTalent))
            );

            /// Remove Crushed Rock Output
            Recipes[0].RemoveOutput(typeof(CrushedSandstoneItem));
        }
    }

}