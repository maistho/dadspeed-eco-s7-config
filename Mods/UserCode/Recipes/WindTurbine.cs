﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;

    public partial class WindTurbineRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Make recipe a lot more expensive
            /// Remove all bonuses from Upgrades/Skill Level
            this.Recipes[0].Ingredients.Clear();
            this.Recipes[0].Ingredients.AddRange(new List<IngredientElement> {
               new IngredientElement(typeof(SteelPlateItem), 100, true),
               new IngredientElement(typeof(SteelGearboxItem), 20, true),
               new IngredientElement(typeof(AdvancedCircuitItem), 80, true),
                /// Add Nylon Fabric to make it more expensive and give some extra business to Tailoring
               new IngredientElement(typeof(NylonFabricItem), 30, true),
                /// Add Epoxy to make it more expensive and give some extra business to Oil Drilling 
               new IngredientElement(typeof(EpoxyItem), 30, true),
               /// Add Fiberglass and Concrete for the pad it's placed on
               new IngredientElement(typeof(FiberglassItem), 25, true),
               new IngredientElement(typeof(ReinforcedConcreteItem), 25, true),
            });

            /// Increase Exp by around 5x to compensate slightly for increased costs
            this.ExperienceOnCraft *= 5;

            /// Increase calories 20x
            this.LaborInCalories = CreateLaborInCaloriesValue(24000, typeof(ElectronicsSkill));

            /// Increase Craft Time 5x
            this.CraftMinutes = CreateCraftTimeValue(typeof(WindTurbineRecipe), 100, typeof(ElectronicsSkill), typeof(ElectronicsFocusedSpeedTalent), typeof(ElectronicsParallelSpeedTalent));
        }
    }
}
