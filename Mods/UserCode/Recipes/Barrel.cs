﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class BarrelRecipe
    {
        partial void ModsPreInitialize()
        {
            Recipes[0].ReplaceIngredient(
                typeof(IronBarItem),
                new IngredientElement(typeof(IronPlateItem), 2, typeof(OilDrillingSkill), typeof(OilDrillingLavishResourcesTalent))
            );
        }
    }
}
