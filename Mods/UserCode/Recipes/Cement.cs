﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;
    public partial class CementRecipe : RecipeFamily
    {
        partial void ModsPreInitialize()
        {
            /// Additional Crushed Rock Usage 
            this.Recipes[0].Ingredients.Add(
                new IngredientElement("Silica", 2, typeof(MasonrySkill), typeof(MasonryLavishResourcesTalent))
            );
        }
    }
}