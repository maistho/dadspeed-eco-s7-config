﻿namespace Eco.Mods.TechTree
{
    using System.Collections.Generic;
    using Eco.Gameplay.Items;

    public partial class SkidSteerRecipe
    {
        partial void ModsPreInitialize()
        {
            /// Increase costs of some ingredients and make them static
            Recipes[0].ReplaceIngredient(
                typeof(GearboxItem),
                new IngredientElement(typeof(SteelGearboxItem), 4, true)
            );
            Recipes[0].ReplaceIngredient(
                typeof(SteelPlateItem),
                new IngredientElement(typeof(SteelPlateItem), 50, true)
            );
            Recipes[0].ReplaceIngredient(
                typeof(NylonFabricItem),
                new IngredientElement(typeof(NylonFabricItem), 40, true)
            );

            /// 3x xp, calories and craft time
            this.ExperienceOnCraft *= 3;
            this.LaborInCalories = CreateLaborInCaloriesValue(7500, typeof(IndustrySkill));
            this.CraftMinutes = CreateCraftTimeValue(typeof(SkidSteerRecipe), 30, typeof(IndustrySkill));
        }
    }
}
