##  (2022-07-14)


##  General Changes

* ### Increase Carrying slots and max weight for vehicles ([33c7e46](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/33c7e464a7d8cdbe8fa3179730bc5a9dc0185988))
  
* ### Increase Fuel Consumption of Vehicles by 5x ([75fba1e](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/75fba1e3d2a575c6723dbc100f35e86e9202294c))  
```
Not touching the Crane since it's already kind of rubbish.
```
  
* ### Increase Skill requirements for Advanced and Modern Research Papers ([8c24532](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/8c24532970757ccb19158193c2d02211af5c9a8e))  
```
Agriculture Advanced 0->1
Agriculture Modern 0->3
Dendrology Advanced 0->2
Dendrology Modern 0->3
Engineering Advanced 1->2
Engineering Modern 1->3
Geology Advanced 0->2
Geology Modern 0->3
Metallurgy Advanced 0->2
Metallurgy Modern 1->3
```
  
* ### Increase Skill requirements for crafting Skill Scrolls ([0cd92f9](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/0cd92f9b87de65557a4d535805ec53bafbd76702))  
```
For any skills that were previously 0, increase to 1
For any skills that were previously 1, increase to 3

Not including Paper Milling
```
  
* ### Set recipes for Skill Books to return Skill Scrolls instead ([5b05601](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/5b05601742d9d3d3478fb4f47d402420e527e0e3))
  


## Adv. Masonry

* ### Replace Steel Bar with 2 Reinforced Concrete ([ea1f334](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/ea1f33403b3cedf511fd78c454b70f412f6cdfe0))
  


## Adv. Smelting

* ### Add 1 Ceramic Mold to Steel Bar Recipe ([e2f2e1f](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/e2f2e1fbbd311353d6432cc73995eb4b77113ab6))  
```
This is meant to bring more business to Pottery and use up more Clay
```
  


## Basic Engineering

* ### Double CrushedRock usage in Asphalt Concrete ([a846d6b](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/a846d6ba7d236191f31b773182ef40144a3ed4e3))
  
* ### Reduce skill level required to make a Powered Cart from 5 to 1 ([3a45624](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/3a45624cc9c7c92e34e18918dbe2564fb81816c6))
  


## Composites

* ### Increase Composite Lumber Material Usage ([e8fb66b](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/e8fb66b7bb58cd6a71f101a204e14ad954db8a54))  
```
Lumber 1->2
Wood  1->4
```
  


## Cutting Edge Cooking

* ### Increase required Cutting Edge Cooking skill level for Ethanol from 0 to 1 ([2a6087d](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/2a6087d3222349d5599812022d8993b45b4fe329))  
```
This is meant to be combined with a law/rule that forbids Gatherers and Oil Drillers from taking Cutting Edge Cooking
```
  


## Electronics

* ### Increase cost of renewable power sources ([515526f](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/515526fe9adfddeb49449906383d8184a9175086))  
```
Wind Turbine:
Increased costs by introducing more expensive materials, now costs
    100 Steel Plate
    20 Steel Gearbox
    80 Advanced Circuit
    30 Nylon Fabric
    30 Epoxy
    25 Fiberglass
    25 Reinforced Concrete
Removed crafting resource reductions from upgrades and skills
Increased experience gained on craft 5x
Increased crafting time 5x
Increased Calorie usage 20x

Solar Generator:
Increased costs by introducing more expensive materials, now costs
    60 Steel Plate
    24 Servo
    24 Basic Circuit
    24 Framed Glass
    40 Gold Wiring
Removed crafting resource reductions from upgrades and skills
Increased Experience gained on craft 5x
Increased crafting time 4x
Increased Calorie usage 4x
```
  


## Farming

* ### Increase dirt cost for Agriculture Research Paper Advanced by 5x ([e98a2b8](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/e98a2b83a525be1f8757b1115172e64d595d59cd))  
```
From 5 -> 25 dirt
```
  


## Glassworking

* ### Reduce skill level required to make AU2 from 4 to 3 ([805e0d5](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/805e0d5c8804d3f4328d072986e712c7d00c3d5d))
  


## Industry

* ### Increase costs of making Skid Steers and Excavators ([dcdf367](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/dcdf367bb241b07213a545cf2486cfe3d0d39fbe))
  


## Logging

* ### Change Charcoal recipe ([3d435b7](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/3d435b739d17e72b0b8cd1946bd4a1fdc4498efd))  
```
- Lower skill requirement from 4 -> 2
- Change from 7 wood-> 1 charcoal to 6 wood -> 2 charcoal
- Increase calorie requirement from 25 to 100
```
  


## Masonry

* ### Add 2 CrushedRock to Cement Recipes ([0e73fb9](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/0e73fb9566c02d235034be126fb397f9a2c5ebce))
  
* ### Add Boiled Grains Mortar Recipe ([15502b3](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/15502b3af4d7c754fe6111424c3fcb63101b1776))  
```
Uses 10 Boiled Grains for 3 Mortar

This is so that we can add some more diversity to farming biomes,
since rice is farmed in different biomes to wheat. It also adds some
business to campfire cooking.
```
  
* ### Increase costs to make non-wheat mortar ([3a6f7c9](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/3a6f7c9839720b5afffa5339a03be1e8a640d19c))  
```
Masonry Mortar is changed to 1 sand -> 1 mortar
Charred Mortar is changed to 20 NaturalFiber -> 1 mortar, and 3x calorie cost
Baked Mortar is changed to 12 WoodPulp -> 1 mortar, and 2x calorie cost
Wheat Mortar still costs 8 wheat -> 3 mortar
```
  
* ### Increase CrushedRock usage in Reinforced Concrete ([a1079b0](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/a1079b0238848577f578ab428807ba59ebc95708))
  


## Mechanics

* ### Add 4 Cotton Fabric to Gold & Copper Wiring recipes ([a1f66b6](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/a1f66b62f57750baab2e1accaf23248da664445a))
  


## mods

* ### Add Oil Prospecting and EM Framework mods ([089fec4](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/089fec4bdd6989d669a8544680496c649b95abf6))
  


## Oil Drilling

* ### Barrel Recipe, Replace 1 Iron Bar with 2 Iron Plates ([d06a73e](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/d06a73e37a4f4bfe2ea3c47436c1bd6202ae53df))  
```
This adds some more business to Mechanics
```
  


## Smelting

* ### Add 1 Ceramic Mold to each Blast Furnace recipe ([1b98f70](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/1b98f70553fd365963f29897d1e0e8f4cf621a8e))  
```
This is meant to bring more business to Pottery and use up more Clay

Recipes are doubled both in costs and outputs
Add 1 Ceramic Mold

Ceramic Mold is a new Tag that contains Round and Square pots
Add 0.5kg weight to pots
Reduce Pot stack size to 10
```
  
* ### Add 3 static Clay to each Bloomery recipe ([adf0020](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/adf0020d943695497047058d15605f9487d49f01))  
```
This is meant to make Bloomery recipes worse compared to Blast Furnace and to use up more Clay
```
  


## Tailoring

* ### Double crafting costs of Carpets ([13b86ea](https://gitlab.com/maistho/dadspeed-eco-s7-config/commit/13b86ea0ac0f8a474d75eac063b8ba06c874efe3))
  


